---
title: Wie bekomme ich einen Freifunk-Knoten?
layout: page
sub_menu: true
sub_weight: 10
permalink: /mitmachen/wie-bekomme-ich-einen-freifunk-knoten/
top_url: /mitmachen/
---
Wir sind eine Community und verkaufen keine Geräte. Trotzdem ist es recht einfach einen oder mehrere konfigurierte Router zu bekommen:

1. Du installierst die frei verfügbare Freifunk-Firmware selbst auf einem vorhandenem und kompatiblen Router. Dazu solltest du über etwas technische Vorkenntnisse verfügen.
2. Du beschaffst dir selbst einen der von uns empfohlenen Router. Danach kommst du mit deinem Gerät zu einem unserer Treffen und wir richten das System mit der Freifunk-Firmware ein.

<i>Text(bearbeitet): [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/de/) by [Freifunk-Aachen.de](http://freifunk-aachen.de)</i>
