---
title: Freifunk Basiswissen
layout: page
sub_menu: true
sub_weight: 30
permalink: /mitmachen/freifunk-basiswissen/
top_url: /mitmachen/
---
Hier gibt es eine kurze, nicht zu technische Übersicht über die Funktion von Freifunk im eigenen Netzwerk.

### Der Ist-Zustand im eigenen Netz (vereinfacht)
In vielen kleinen Netzwerken, Zuhause oder in der Gastronomie, arbeitet ein WLAN Router. Dieser ist via DSL, V-DSL, Kabel oder einer ähnlichen Technologien mit dem Internet verbunden. Die Seite mit der Internetanbindung nennt man WAN ([Wide Area Network](https://de.wikipedia.org/wiki/Wide_Area_Network/){:target="_blank"}).

Auf der anderen Seite des Routers befindet sich das sogenannte LAN ([Local Area Network](https://de.wikipedia.org/wiki/Local_Area_Network){:target="_blank"}). Dort ist man entweder direkt mit einem Netzwerkkabel oder über das eigene Funknetz (WLAN) mit dem Router verbunden. Das WLAN ist im Regelfall gegen Benutzung durch Dritte verschlüsselt.

{% include image.html url="/images/pages/01-lokales-netz-ist-zustand.png" description="Lokales Netz Ist-Zustand" %}

### Freifunk-Router beschaffen
Die Freifunker verwenden eine Reihe ganz bestimmter und getesteter WLAN-Router und WLAN-Hardware. Diese müssen zur Teilnahme am Freifunk mit einem neuen Betriebssystem (Firmware) versehen werden. Die Software wird von der sehr aktiven Freifunk-Gemeinde stetig entwickelt und gepflegt. Man kann die Software selber aufspielen (flashen). Der Preis für ein gutes Standardgerät startet bei ca. 15€. Alternativ bringt man einen empfohlenen Router zu einem der Treffen mit und erhält die notwendige Hilfestellung beim Aufspielen der Freifunk-Firmware.
Am zukünftigen Standort angekommen sind nur noch zwei Dinge zu tun. Die (meistens blaue) WAN-Buchse des Freifunk-Routers muss mittels des beiliegenden Netzwerkkabels mit einer (meistens gelben) LAN-Buchse des bereits vorhandenen Routers verbunden werden. Der Freifunk-Router wird dann noch mit dem beiliegenden Netzteil mit dem Strom verbunden und eingeschaltet. Es dauert ca. 1 bis 2 Minuten bis der Router gestartet ist. Fertig!

{% include image.html url="/images/pages/02-freifunk-router-angeschlossen.png" description="Mit angeschlossenem Freifunk-Knoten" %}

### Wie funktioniert das dann?
Der Freifunk Router ist durch eine integrierte Firewall vom eigenen, privaten Netzwerk getrennt. Es ist nicht möglich vom Freifunk-Router eine Verbindung in das private lokale Netzwerk aufzubauen. Die einzig mögliche Verbindung ist der Aufbau eines sogenannten [VPN Tunnels](https://de.wikipedia.org/wiki/Virtual_Private_Network){:target="_blank"} zu unseren Freifunk-Servern.

Die gesamte Kommunikation des Freifunk-Routers läuft durch diesen Tunnel. Auch die Zuweisung einer Netzwerk-Adresse ([IP](https://de.wikipedia.org/wiki/IP-Adresse){:target="_blank"}) durch einen unserer Server erfolgt durch diesen Tunnel. Dadurch erhalten Freifunk-Gastnutzer eine IP-Adresse die dem Freifunk Rheinland e. V. zugeordnet ist und nichts mit dem eigenen, privaten Internetzugang zu tun hat. Der eigentliche Internetzugang wird  ausschließlich von den Freifunk-Servern zur Verfügung gestellt.

Bricht aus irgendeinem Grund die Tunnelverbindung zusammen (z. B. weil die Freifunk-Server nicht erreichbar sind), dann kommt am Freifunk-Router keinerlei Verbindung zustande. D. h. im Fehlerfall kann __keine__ Kommunikation eines Freifunk-Gastes über den eigenen, privaten Router erfolgen. Entweder läuft der Datenverkehr über die Freifunk-Server oder es funktioniert gar nichts. Im eigenen Netz ändert sich nichts und es sind keine Veränderungen bemerkbar.

{% include image.html url="/images/pages/03-freiffunk-router-mit-gaesten.png" description="Freifunk Router mit Gästen" %}

<i>Text(bearbeitet): [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/de/) by [Freifunk-Aachen.de](http://freifunk-aachen.de)</i>  
<i>Netzwerkdiagramm: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/de/) by Felix Bosseler</i>
