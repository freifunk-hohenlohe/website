---
title: Freifunk Zuhause nutzen
layout: page
sub_menu: true
sub_weight: 40
permalink: /mitmachen/freifunk-zuhause-nutzen/
top_url: /mitmachen/
---
{% include image.html url="/images/pages/zuhause.png" description="Freifunk Zuhause nutzen" %}

Jeder der ein WLAN zu Hause betreibt kennt das: Die lieben Verwandten oder Freunde sind zu Besuch und es dauert nicht lange, dann kommt die obligatorische Frage nach dem WLAN-Passwort und das „Gefrickel“ am Smartphone der Gäste beginnt.

Neuere Router bieten den Vorteil eines Gäste WLANs. Das ist dann auch vom eigenen WLAN getrennt nutzbar. Aber auch da muss der Schlüssel gesucht, gefunden und eingegeben werden und die Gäste surfen mit der Adresskennung ([IP](https://de.wikipedia.org/wiki/IP-Adresse){:target="_blank"}) des Anschlussinhabers.

Mit einem zusätzlichen Freifunk-Router ([gute Geräte](/technik/empfohlene-hardware/) gibt es bereits ab ca. 15 €) entfallen alle diese Probleme. Mehr noch: Man verbreitet die Idee eines freien Netzes und hilft, bei entsprechender Reichweite, auch noch sämtlichen Nachbarn und Mitbewohnern das Thema „WLAN für alle“ zu lösen.

Ein Einfaches „nimm das Netz mit dem Namen Freifunk“ reicht. Keine Anmeldung, kein Passwortgefummel. Einfach auswählen und los.

Im Bild oben schematisch dargestellt: Der Freifunk-Router (purpurfarben) spannt ein WLAN mit der Kennung „Freifunk“ auf. Der Datenverkehr über dieses Netz wird durch die interne Firewall des Freifunk-Routers zu unseren Freifunk-Servern getunnelt. Von dort geht der Datenverkehr dann erst in das Internet und zurück.

Das eigene Netz bzw. WLAN (blau) wird nicht verändert und ist vom Freifunk entkoppelt. Die Bandbreite die für Freifunk max. genutzt werden soll, kann begrenzt werden. Beispielsweise werden an einem 16.000 Kbit/s Anschluss 3.000 Kbit/s für Freifunk eingestellt, dann verbleiben immer mindestens 13.000 Kbit/s bis max. 16.000 Kbit/s für eigene Zwecke.

### Vorteile
* Durch die interne Firewall und dem verwendeten VPN-Tunnel ist der Freifunk-Router vom eigenen Netz entkoppelt. Ein Zugriff auf andere Rechner des eigenen Netzes ist nicht möglich.
* Auch Nachbarn und deren Gäste genießen freies Netz im ganzen Haus (reichweitenabhhängig, aber erweiterbar)
* Der Stromverbrauch des zusätzlichen Routers liegt zwischen gemessenen 2 bis 6 Watt.
* Man hilft die Idee eines freien Netzes weiter zu verbreiten
* Die Bandbreite, welche die Benutzer des Freifunk Router max. für Up- oder Downstream nutzen können, wird dabei einmalig vorher eingestellt.
* Keine Passworteingabe, keine Landingpages,  keine Codes, einfach mit dem Freifunk-WLAN verbinden und sofort loslegen.

Weitere Hintergrundinformationen zur Funktionsweise gibt es unter [Freifunk Basiswissen](/mitmachen/freifunk-basiswissen/).

<i>Text(bearbeitet): [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/de/) by [Freifunk-Aachen.de](http://freifunk-aachen.de)</i>
<i>Netzwerkdiagramm: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/de/) by Felix Bosseler</i>
