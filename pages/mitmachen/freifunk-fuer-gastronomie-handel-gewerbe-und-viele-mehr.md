---
title: Freifunk für Gastronomie, Handel, Gewerbe und viele mehr
layout: page
sub_menu: true
sub_weight: 50
permalink: /mitmachen/freifunk-fuer-gastronomie-handel-gewerbe-und-viele-mehr/
top_url: /mitmachen/
---
{% include image.html url="/images/pages/freifunk-gewerbe-handel-mesh.png" description="Freifunk für Gastronomie, Handel, Gewerbe und viele mehr" %}

Gästen und Kunden einfach WLAN zur Verfügung stellen? Freifunk! Damit lösen Sie die Probleme die dieses Vorhaben manchmal mit sich bringt.

### Technik
Sie benötigen lediglich einen Internetanschluss mit einer freien Netzwerkbuchse an einem Internet-Router und eine 24/7 verfügbare Steckdose zur ununterbrochenen Stromversorgung. Kleine Ladenlokale ohne Besonderheiten wie z. B. ein verwinkelter Grundriss sind im Aufwand auf dem gleichen Level wie die Installation im privaten [Zuhause](/mitmachen/freifunk-zuhause-nutzen/).

Sollen zusätzliche Räumlichkeiten oder z.B. eine Außengastronomie ebenfalls mit Freifunk versorgt werden, dann benötigen Sie einen oder mehrere zusätzliche Freifunk-Router. Die weiteren Router müssen aber nicht unbedingt auch per Kabelverbindung am  Router mit Internet-Anschluss angeschlossen werden, da Freifunk-Router standardmäßig auch WLAN-Verbindungen untereinander aufbauen können. Eine 24/7 verfügbare Steckdose reicht für die Erweiterung mit einem weiteren Router meistens aus.

Die Möglichkeit, dass Freifunk-Router WLAN-Verbindungen untereinander aufbauen können, ist ein Kernelement von Freifunk.  Diese besondere Funktion der von den Freifunk-Mitgliedern ständig weiterentwickelten Firmware macht erst ein eigenständiges Freifunk-Netz möglich. Stehen zwei oder mehr Router in einer Entfernung die eine WLAN Verbindung ermöglicht, dann verbinden sich diese Freifunk-Router automatisch miteinander. Das nennt sich „Meshing“ oder „[Vermaschen](https://de.wikipedia.org/wiki/Vermaschtes_Netz){:target="_blank"}“.

Im obigen Bild erkennt man zwei Freifunk-Router. Der erste hat einen Internet-Zugang über den hauseigenen Router. Der zweite hat mit dem ersten eine Mesh-Verbindung hergestellt. Beide senden die WLAN-Kennung (SSID) „Freifunk“ aus. Verbindet sich ein Benutzer mit dem Freifunk-Router Nr. 2, dann wickelt dieser seinen Datenverkehr über den Router Nr. 1 ab.

Die Anzahl der Router ist nur durch die Auslastung der WLAN-Kanäle, Speicher im Freifunk-Router und CPU-Ressourcen im Freifunk-Router begrenzt.

Verbinden sich Freifunk-Router untereinander, die auch eine Internetverbindung haben (so wie Freifunk-Router Nr. 1), dann findet eine Lastverteilung statt, um das Freifunk-Netz gleichmäßig zu nutzen. Dieses Netzwerk funktioniert über Etagen und Gebäudegrenzen hinweg. Mittels professioneller WLAN-Technik können auch größere Plätze abgedeckt oder direkte Verbindungen zwischen Stadtteilen aufgebaut werden.

### Betriebsaufwand
Die Hardware ist kostengünstig (Router ab 15 €, [empfohlene Systeme](/technik/empfohlene-hardware/) für Umgebungen mit einer größeren Nutzerzahl ab ca. 45 bis 60 € brutto). Fällt ein Router aus, dann kann dieser einfach durch einen beliebigen Freifunk-Router ersetzt werden. In kritischen Umgebungen kann durch Bevorraten eines fertig konfigurierten Geräts schneller Ersatz unkompliziert vorgehalten werden. Im Fehlerfall kann ein Router durch einfaches Umstecken (1 x Netzteil, ggf. 1 x Netzwerkstecker) auch von Laien ausgetauscht werden. [Router erwerben und bespielen Sie selber](/mitmachen/wie-bekomme-ich-einen-freifunk-knoten/).

Ihre Mitarbeiter müssen sich nicht um das Freifunk-WLAN kümmern. Sie benötigen keine Anmeldedaten, Gutscheine oder Vouchers, die Sie an Kunden ausgeben oder ihnen erklären müssen. Es gibt keine zwangsweise Umleitung zu einer Internetseite, die möglicherweise erst vom Benutzer „überwunden“ werden muss. Einfach mit dem WLAN mit „Freifunk“ im Namen verbinden und loslegen.

Da Geräte sich diese sog. SSID merken, muss der Kunde oder Gast diese nur ein einziges Mal manuell auswählen. Schon beim nächsten Besuch (egal wo sich Freifunk-Router befinden) verläuft die Anmeldung ohne Zutun des Benutzers.

### Vorteile
* Durch die interne Firewall und dem verwendeten VPN-Tunnel ist der Freifunk-Router vom eigenen Netz entkoppelt. Ein Zugriff auf andere Rechner des eigenen Netzes ist nicht möglich.
* Der eigentliche Internetzugriff erfolgt durch den Tunnel über Server des Freifunk Rheinland e. V. Benutzer verwenden dabei eine IP-Kennung des Vereins, nicht des Anschlussinhabers.
* Der Stromverbrauch der Freifunk-Router liegt je nach Modell zwischen 2 und 6 Watt (gemessen). Weitere Kosten gibt es nicht.
* Praktisch zu vernachlässigende Einstiegskosten, keine mtl. Kosten. Es sei denn Sie möchten den Verein durch Einzelspende oder Mitgliedschaft unterstützen. Das ist aber keine Voraussetzung!
* Sie unterstützen die Idee eines freien Netzes.
* Die Bandbreite, welche die Benutzer der Freifunk-Router max. nutzen können, kann begrenzt werden. Ihr eigentlicher Internet-Anschluss hat für Sie die eingestellte, garantierte Bandbreite.
* Wir unterstützen Sie kostenfrei und ehrenamtlich bei der Planung und dem Betrieb der Systeme.
* Keine Gutscheine/Voucher: Ihr Mitarbeiter müssen keine Voucher ausgeben, verkaufen oder bei Problemen den Kunden unterstützen.
* Keine zwangsweise Umleitung zu einer Portalseite (Anmeldehemmnis mit möglichen Rückfragen an Ihre Mitarbeiter).
* Freies Internet ohne zeitliche Begrenzung.
* Nahezu freie Wahl des Routernamens. z. B. „ffhoh-Pizzeria-TresTomates“. Der Namen erscheint so auf unserer [Hotspotkarte](/karte/).
* Wettbewerbsvorteil gegenüber Mitbewerbern, die kein offenes, kostenfreies WLAN ohne zeitliche Beschränkung anbieten.

### Für wen
* Bäckerei oder Metzgerei mit Gastbereich
* Bistro
* Imbiss
* Kneipen und Gaststätten
* Restaurant
* Hotel
* Friseur
* Sonnenstudio
* Arztpraxis
* Kanzlei
* Einzelhandel
* Werkstatt
* Möbelhaus
* Temporär bei Veranstaltungen
* Kongresszentrum
* Bibliothek
* Jugendherberge
* Pfarrheim
* Wartebereiche bei Behörden und Verwaltungen
* Kleingärtnerverein
* ...

Weitere Hintergrundinformationen zur Funktionsweise gibt es unter [Freifunk Basiswissen](/mitmachen/freifunk-basiswissen/).

Außer für die Anschaffung und den Internetzugang entstehen keine weiteren Kosten. Natürlich wünschen wir uns, dass die Community unterstützt wird. Sei es durch [einmalige Spende oder durch eine Mitgliedschaft](/mitmachen/die-idee-interstuetzen/). Das ist aber keine Voraussetzung!

Sie haben Interesse an einer Vorstellung des Konzepts in Ihrem Haus? Fraktion? Verwaltung? Projektgruppe? Interessengruppe? [Kontaktieren Sie uns!](/kontakt/) Wir kommen gerne zu Ihnen, stellen das Projekt vor und stehen Rede und Antwort.

<i>Text(bearbeitet): [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/de/) by [Freifunk-Aachen.de](http://freifunk-aachen.de)</i>  
<i>Netzwerkdiagramm: [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/de/) by Felix Bosseler</i>
