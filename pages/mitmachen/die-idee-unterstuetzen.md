---
title: Die Idee unterstützen
layout: page
sub_menu: true
sub_weight: 20
permalink: /mitmachen/die-idee-interstuetzen/
top_url: /mitmachen/
---
Auch wenn wir unsere Arbeit rein ehrenamtlich und unentgeltlich verrichten, reicht es nicht.
Es müssen Router, Antennen, Kabel, und noch vieles mehr beschafft werden um ein möglichst großes Freifunknetz aufzubauen.
Es gibt viele Möglichkeiten die Verbreitung freier Netzwerke zu unterstützen. Beispielsweise: Unterstütze das Projekt fallweise in der lokalen Community. Teile deinen Internetzugang. Stelle einen oder mehrere Freifunk-Router auf. Gründe mit anderen selber eine Community in deiner Stadt oder Gemeinde. Spreche mit anderen Menschen über Freifunk. Verbreite die Idee.

### Spenden
Wir nutzen die Infrastruktur von Freifunk Rheinland. Infos zum direkten Spenden auf [freifunk-rheinland.net](https://www.freifunk-rheinland.net/mitmachen/spenden/).

Eine Übersicht über viele Projekte bietet auch [freifunk.net](https://freifunk.net/wie-mache-ich-mit/spende-fuer-die-projekte/).

Wir als Freifunk Hohenlohe haben noch keine eigene "Spendeninfrastrukur". Wenn du uns trotzdem direkt mit Sach- oder Geldspenden unterstüten willst, dann wende dich direkt an uns (siehe [Kontakt](/kontakt)).

<i>Text(bearbeitet): [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/de/) by [Freifunk-Aachen.de](http://freifunk-aachen.de)</i>
