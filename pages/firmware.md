---
title: Firmware
layout: page
main_menu: true
weight: 30
permalink: /firmware/
---
Nicht jeder WLAN-Router kann für Freifunk benutzt werden, da sie ohne die spezielle Firmware nicht im Freifunk-Netz teilnehmen können.
Für jedes Modell gibt es eine eigene Firmware. Am einfachsten auf der Download-Seite nachschauen (und dabei auch genau auf die Versionsnummern achten):

[Firmware Download Übersicht](https://firmware.freifunk-hohenlohe.de/firmware/stable/factory/)
