---
title: Technik
layout: page
main_menu: true
sub_menu: true
weight: 50
permalink: /technik/
top_url: /technik/
---
### Empfohlene Hardware
Welche Hardware benötigen Sie für Freifunk?
[Weiterlesen&nbsp;<span class="meta-nav">→</span>](/technik/empfohlene-hardware/)

### Freifunk-Knoten
Ein Knoten (oder auch Node) ist der Zugangspunkt zum Freifunk-Netz.
[Weiterlesen&nbsp;<span class="meta-nav">→</span>](/technik/freifunk-knoten/)

### Freifunk-Netzstruktur
Ein technischer Blick auf die Freifunk Infrastruktur.
[Weiterlesen&nbsp;<span class="meta-nav">→</span>](/technik/freifunk-netzstruktur/)
