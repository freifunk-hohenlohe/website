---
title: Mitmachen
layout: page
main_menu: true
sub_menu: true
weight: 40
permalink: /mitmachen/
top_url: /mitmachen/
---
### Wie bekomme ich einen Freifunk-Knoten?
Wir sind eine Community und verkaufen keine Geräte. Trotzdem ist es recht einfach einen oder mehrere konfigurierte Router zu bekommen.
[Weiterlesen&nbsp;<span class="meta-nav">→</span>](/mitmachen/wie-bekomme-ich-einen-freifunk-knoten/)

### Die Idee unterstützen
Auch wenn wir unsere Arbeit rein ehrenamtlich und unentgeltlich verrichten, reicht es nicht.
[Weiterlesen&nbsp;<span class="meta-nav">→</span>](/mitmachen/die-idee-interstuetzen/)

### Freifunk Basiswissen
Hier gibt es eine kurze, nicht zu technische Übersicht über die Funktion von Freifunk im eigenen Netzwerk.
[Weiterlesen&nbsp;<span class="meta-nav">→</span>](/mitmachen/freifunk-basiswissen/)

### Freifunk Zuhause nutzen
Jeder der ein WLAN zu Hause betreibt kennt das: Die lieben Verwandten oder Freunde sind zu Besuch und es dauert nicht lange, dann kommt die obligatorische Frage nach dem WLAN-Passwort.
[Weiterlesen&nbsp;<span class="meta-nav">→</span>](/mitmachen/freifunk-zuhause-nutzen/)

### Freifunk für Gastronomie, Handel, Gewerbe und viele mehr
Gästen und Kunden einfach WLAN zur Verfügung stellen?
[Weiterlesen&nbsp;<span class="meta-nav">→</span>](/mitmachen/freifunk-fuer-gastronomie-handel-gewerbe-und-viele-mehr/)

### Nutzungbedingungen
Das Pico Peering Agreement.
[Weiterlesen&nbsp;<span class="meta-nav">→</span>](/mitmachen/nutzungbedingungen/)
