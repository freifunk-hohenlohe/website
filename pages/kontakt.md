---
title: Kontakt
layout: page
main_menu: true
sub_menu: true
weight: 60
permalink: /kontakt/
top_url: /kontakt/
---
### E-Mail
info&nbsp;at&nbsp;freifunk-hohenlohe.de

### Telefon
+49&nbsp;7904&nbsp;2125692  
Bitte auf die Mailbox sprechen.
