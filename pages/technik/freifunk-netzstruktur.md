---
title: Freifunk-Netzstruktur
layout: page
sub_menu: true
sub_weight: 30
permalink: /technik/freifunk-netzstruktur/
top_url: /technik/
---
Ein technischer Blick auf die Freifunk Infrastruktur.

Das Netz von Freifunk Rheinland e.V. und damit auch das Freifunk-Netz für Hohenlohe ist in mehreren Ebenen strukturiert. Eine Übersicht gibt es im Wiki des Freifunk Rheinland e.V.: [https://wiki.freifunk-rheinland.net/wiki/Freifunk_Rheinland_Net_2.0](https://wiki.freifunk-rheinland.net/wiki/Freifunk_Rheinland_Net_2.0){:target="_blank"}

Wie man dort nachlesen kann, gliedert sich das Freifunk Netz in mehrere Ebenen.

1. Es gibt Clients, die sich mit den Nodes (auch Freifunk-Router gennant) verbinden.
2.  a) Es gibt Nodes, die keinen Internetanschluss besitzen und sich mit benachbarten Nodes verbinden (diese Verbindung ist in der Grafik nicht dargestellt).

    b) Es gibt Nodes, die einen Internetanschluss besitzen und über einen Tunnel Verbindungen zu zwei Super-Nodes erstellen.
3. Die Super-Nodes unterhalten gleichzeitig Verbindung untereinander und zu einem Backbone.
4. Der Backbone unterhält Verbindungen zu anderen Freifunk-Netzwerken in Deutschland und zum Internet.

{% include image.html url="/images/pages/freifunk-rheinland-net-2.0.png" description="https://wiki.freifunk-rheinland.net/wiki/Freifunk_Rheinland_Net_2.0" %}

Freifunk Rheinland ist in unserem Fall der Betreiber der Ebenen 2 bis 4 und die eigentliche Internetverbindung wird erst beim Backbone erstellt. Damit ist man als Anschlussinhaber aus der Verantwortung.
Sollte aus irgendeinem Grund der Tunnel zwischen Node und Super-Node zusammenbrechen, ist der Node von jeglicher Kommunikation abgeschnitten. Es kann also nicht zu einer Situation kommen, dass plötzlich Freifunk direkt ins Internet geleitet wird. Damit ist man vor der Störerhaftung geschützt.

<i>Text(bearbeitet): [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/de/) by [Freifunk-Aachen.de](http://freifunk-aachen.de)</i>
