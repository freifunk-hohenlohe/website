---
title: Empfohlene Hardware
layout: page
sub_menu: true
sub_weight: 10
permalink: /technik/empfohlene-hardware/
top_url: /technik/
---
Welche Hardware benötigen Sie für Freifunk? Welche wird für Drinnen oder Draußen empfohlen? Für den schnellen und reibungslosen Start haben wir eine Auswahl der möglichen WLAN Hardware und Router zusammengestellt.

__Achtung__: Vor dem Kauf immer auf die Versionsnummer auf der Verpackung achten! __Wenn die Hardwareversion nicht übereinstimmt, dann gibt es keine Freifunk-Firmware dafür und der Router kann nicht verwendet werden!__

<table>
  <tr>
    <th></th>
    <th>TP-Link TL-WR841N</th>
  </tr>
  <tr>
    <td>Standort</td>
    <td>Innen</td>
  </tr>
  <tr>
    <td>WAN</td>
    <td>1x 100 MBit</td>
  </tr>
  <tr>
    <td>LAN</td>
    <td>4x 100 MBit</td>
  </tr>
  <tr>
    <td>WLAN</td>
    <td>2,4 GHz bis 300MBit</td>
  </tr>
  <tr>
    <td>Empfehlung</td>
    <td>Privathaushalte</td>
  </tr>
  <tr>
    <td>Preis</td>
    <td>ab 15€</td>
  </tr>
</table>

Die Freifunk-Entwickler unterstützen darüberhinaus eine ganze Reihe anderer Routerhardware. Mehr dazu unter [Firmware](/firmware/).

Für spezielle Anwendungen wie z. B. das Ausleuchten ganzer Plätze, kann professionelle WLAN Hardware zur Steigerung von Reichweite und Abdeckung zum Einsatz kommen. Wir helfen gerne bei der Planung.

<i>Text(bearbeitet): [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/de/) by [Freifunk-Aachen.de](http://freifunk-aachen.de)</i>
