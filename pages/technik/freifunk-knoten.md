---
title: Freifunk-Knoten
layout: page
sub_menu: true
sub_weight: 20
permalink: /technik/freifunk-knoten/
top_url: /technik/
---
Ein Knoten (oder auch Node) ist der Zugangspunkt zum [Freifunk-Netz](/technik/freifunk-netzstruktur/). Einen solchen Knoten selber zu konfigurieren und einzurichten ist nicht schwer. Wir tragen hier alle nötigen Informationen zusammen.

Die aktuelle Firmware findest Du unter [Firmware](/firmware/). Achte peinlichst genau auf die genaue Bezeichnung des Routers und eine eventuell angegebene Versionsnummer.

Eine Anleitung zum Einspielen der Firmware von der Original-Firmware aus findet ihr unter:
[http://wiki.freifunk.net/Freifunk_Aachen/Softwareinstallation](http://wiki.freifunk.net/Freifunk_Aachen/Softwareinstallation)

Sowie zum Einrichten der Firmware:
[http://wiki.freifunk.net/Freifunk_Aachen/Softwareinstallation#Router_konfigurieren](http://wiki.freifunk.net/Freifunk_Aachen/Softwareinstallation#Router_konfigurieren)

Hierbei ist zu beachten, dass die Geo-Koordinaten in unserer Version der Firmware händisch beim Setup eingegeben werden. Daher solltest du dir die Koordinaten deines Routers vorher notieren. Die Koordinaten erhältst du ganz leicht über unsere Karte und dem PinNadel-Button.

Eine ganz einfache Sache und inklusive Auspacken des Routers in unter 15 Minuten erledigt. Weitere Informationen auch im Wiki des Freifunk Rheinland e.V.

<i>Text(bearbeitet): [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/de/) by [Freifunk-Aachen.de](http://freifunk-aachen.de)</i>
