# website www.freifunk-hohenlohe.de
* production: [www.freifunk-hohenlohe.de](https://www.freifunk-hohenlohe.de/)
* staging: [staging.freifunk-hohenlohe.de](https://staging.freifunk-hohenlohe.de/)

## Dependencies
* `apt-get install jekyll`

## Workflow
1. Get sources
  * `git clone git@gitlab.com:freifunk-hohenlohe/website.git`
1. Use staging branch for new stuff
  * `git checkout staging`
1. Modify sources
  * vim
  * nano
  * gimp
  * inkscape
  * ...
1. Build Website with jekyll
  * `jekyll build` This will build the website to the `_site` directory.
1. Serving on localhost
  * `jekyll serve` This watches files for changes and serves the website on http://127.0.0.1:4000/
1. commit changes (you can do multiple commits)
  * `git add <file> <file> ... && git commit`
  * or
  * `git commit -a`
1. If somone has change the remote branch rebase!
 * `git pull --rebase`
1. Push changes and verify them on [staging.freifunk-hohenlohe.de](https://staging.freifunk-hohenlohe.de/)
  * `git push`
  * Wait at least 5 Minutes for building an deploying. After deploying an email is sent to you.
1. If evrything is fine, rebase on master, merge and push
  * `git rebase master`
  * `git checkout master`
  * `git merge staging`
  * `git push`
1. Your modifications are now on [www.freifunk-hohenlohe.de](https://www.freifunk-hohenlohe.de/)
